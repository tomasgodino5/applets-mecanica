# se puede correr asi
# gawk -f pendulo.awk

# o correrlo desde gnuplot asi
#gnuplot> plot '< gawk -f pendulo.awk' u 1:2, '' u 1:3, '' u 1:4, 0

BEGIN{
	g=9.8;
	m=0.1;
	l=1.0;

	t=0;
	dt=0.005;

	# condicion inicial
	x=3.14;
	v=0.0;

	for(i=0;i<5000;i++)
	{
		# energia 
		E = m*l**2*v**2*0.5 - m*g*l*cos(x)

		# metodo leapfrog (el mas simple)
		v = v - dt*(g/l)*sin(x)
		x = x + v*dt	
	
		t=t+dt;

		if(i%5==0){print t,x,v,E;}
	}
}

