(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      5555,        173]
NotebookOptionsPosition[      5173,        155]
NotebookOutlinePosition[      5506,        170]
CellTagsIndexPosition[      5463,        167]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Lagrangiana", " ", "=", " ", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{
        RowBox[{
         SuperscriptBox[
          RowBox[{
           RowBox[{"\[Theta]", "'"}], "[", "t", "]"}], "2"], " ", 
         SuperscriptBox["b", "2"]}], "+", 
        RowBox[{
         SuperscriptBox["a", "2"], " ", 
         SuperscriptBox["\[Omega]", "2"]}], "+", 
        RowBox[{"2", " ", 
         RowBox[{
          RowBox[{"\[Theta]", "'"}], "[", "t", "]"}], " ", "a", " ", "b", " ",
          "\[Omega]", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{
           RowBox[{"t", " ", "\[Omega]"}], "-", 
           RowBox[{"\[Theta]", "[", "t", "]"}]}], "]"}]}]}], ")"}], 
      RowBox[{"m", "/", "2"}]}], "+", 
     RowBox[{"m", " ", "g", " ", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"a", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"\[Omega]", " ", "t"}], "]"}]}], "+", 
        RowBox[{"b", " ", 
         RowBox[{"Cos", "[", 
          RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}]}], ")"}]}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{"EulerEquations", "[", 
    RowBox[{"Lagrangiana", ",", 
     RowBox[{"\[Theta]", "[", "t", "]"}], ",", "t"}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.7773297978076143`*^9, 3.777329798164633*^9}, {
   3.777329912545086*^9, 3.777329915652165*^9}, 3.777330268913458*^9, {
   3.777330366163066*^9, 3.777330387695374*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"g", " ", "m", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{"a", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"t", " ", "\[Omega]"}], "]"}]}], "+", 
     RowBox[{"b", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}]}], ")"}]}], "+", 
  RowBox[{
   FractionBox["1", "2"], " ", "m", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      SuperscriptBox["a", "2"], " ", 
      SuperscriptBox["\[Omega]", "2"]}], "+", 
     RowBox[{"2", " ", "a", " ", "b", " ", "\[Omega]", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{
        RowBox[{"t", " ", "\[Omega]"}], "-", 
        RowBox[{"\[Theta]", "[", "t", "]"}]}], "]"}], " ", 
      RowBox[{
       SuperscriptBox["\[Theta]", "\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}], "+", 
     RowBox[{
      SuperscriptBox["b", "2"], " ", 
      SuperscriptBox[
       RowBox[{
        SuperscriptBox["\[Theta]", "\[Prime]",
         MultilineFunction->None], "[", "t", "]"}], "2"]}]}], 
    ")"}]}]}]], "Output",
 CellChangeTimes->{
  3.7773297991270857`*^9, 3.777329846804937*^9, 3.7773298833093576`*^9, {
   3.7773299167981167`*^9, 3.7773299443521442`*^9}, 3.7773299794497223`*^9, 
   3.777330174831195*^9, 3.777330232266205*^9, {3.777330269956472*^9, 
   3.777330279725268*^9}, 3.777330389465164*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"-", "b"}], " ", "m", " ", 
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "a"}], " ", 
      SuperscriptBox["\[Omega]", "2"], " ", 
      RowBox[{"Sin", "[", 
       RowBox[{
        RowBox[{"t", " ", "\[Omega]"}], "-", 
        RowBox[{"\[Theta]", "[", "t", "]"}]}], "]"}]}], "+", 
     RowBox[{"g", " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}], "+", 
     RowBox[{"b", " ", 
      RowBox[{
       SuperscriptBox["\[Theta]", "\[Prime]\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}]}], ")"}]}], "\[Equal]", 
  "0"}]], "Output",
 CellChangeTimes->{
  3.7773297991270857`*^9, 3.777329846804937*^9, 3.7773298833093576`*^9, {
   3.7773299167981167`*^9, 3.7773299443521442`*^9}, 3.7773299794497223`*^9, 
   3.777330174831195*^9, 3.777330232266205*^9, {3.777330269956472*^9, 
   3.777330279725268*^9}, 3.777330389466363*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SuperscriptBox["\[Theta]", "\[Prime]\[Prime]",
     MultilineFunction->None], "[", "t", "]"}], "=", 
   RowBox[{
    RowBox[{
     RowBox[{"(", 
      RowBox[{"a", "/", "b"}], ")"}], " ", 
     SuperscriptBox["\[Omega]", "2"], " ", 
     RowBox[{"Sin", "[", 
      RowBox[{
       RowBox[{"t", " ", "\[Omega]"}], "-", 
       RowBox[{"\[Theta]", "[", "t", "]"}]}], "]"}]}], "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"g", "/", "b"}], ")"}], " ", 
     RowBox[{"Sin", "[", 
      RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], " "}]}]}]}]], "Input",
 CellChangeTimes->{{3.777330291329166*^9, 3.7773302919511147`*^9}, {
  3.777330322329711*^9, 3.777330325095931*^9}, {3.7773306510946093`*^9, 
  3.7773307069778223`*^9}}]
},
WindowSize->{1301, 744},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1472, 40, 80, "Input"],
Cell[2055, 64, 1339, 37, 49, "Output"],
Cell[3397, 103, 936, 25, 37, "Output"]
}, Open  ]],
Cell[4348, 131, 821, 22, 78, "Input"]
}
]
*)

(* End of internal cache information *)
