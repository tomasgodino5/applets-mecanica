(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     38654,        846]
NotebookOptionsPosition[     37618,        806]
NotebookOutlinePosition[     37953,        821]
CellTagsIndexPosition[     37910,        818]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"x", "[", "t", "]"}], "=", " ", 
    RowBox[{
     RowBox[{"Cos", "[", 
      RowBox[{"\[CapitalOmega]", " ", "t"}], "]"}], "+", 
     RowBox[{
      RowBox[{"Sin", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], 
      RowBox[{"Cos", "[", 
       RowBox[{"\[CapitalOmega]", " ", "t"}], "]"}]}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"y", "[", "t", "]"}], "=", " ", 
    RowBox[{
     RowBox[{"Sin", "[", 
      RowBox[{"\[CapitalOmega]", " ", "t"}], "]"}], "+", 
     RowBox[{
      RowBox[{"Sin", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], 
      RowBox[{"Sin", "[", 
       RowBox[{"\[CapitalOmega]", " ", "t"}], "]"}]}]}]}], 
   "\[IndentingNewLine]", 
   RowBox[{
    RowBox[{"z", "[", "t", "]"}], "=", " ", 
    RowBox[{"Cos", "[", 
     RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}], 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.7798167307429943`*^9, 3.7798168808910713`*^9}, {
  3.7798173147603493`*^9, 3.779817319324397*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Cos", "[", 
   RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], "+", 
  RowBox[{
   RowBox[{"Cos", "[", 
    RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}]}]], "Output",
 CellChangeTimes->{3.77981688512421*^9, 3.779817319936172*^9}],

Cell[BoxData[
 RowBox[{
  RowBox[{"Sin", "[", 
   RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], "+", 
  RowBox[{
   RowBox[{"Sin", "[", 
    RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
   RowBox[{"Sin", "[", 
    RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}]}]], "Output",
 CellChangeTimes->{3.77981688512421*^9, 3.779817319937366*^9}],

Cell[BoxData[
 RowBox[{"Cos", "[", 
  RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]], "Output",
 CellChangeTimes->{3.77981688512421*^9, 3.779817319938702*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    RowBox[{"(", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"x", "[", "t", "]"}], ",", "t"}], "]"}], ")"}], "^", "2"}], 
   "+", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"y", "[", "t", "]"}], ",", "t"}], "]"}], ")"}], "^", "2"}], 
   "+", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{"D", "[", 
      RowBox[{
       RowBox[{"z", "[", "t", "]"}], ",", "t"}], "]"}], ")"}], "^", 
    "2"}]}]}]], "Input",
 CellChangeTimes->{{3.779816887043841*^9, 3.7798169326640244`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox[
    RowBox[{"Sin", "[", 
     RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], "2"], " ", 
   SuperscriptBox[
    RowBox[{
     SuperscriptBox["\[Theta]", "\[Prime]",
      MultilineFunction->None], "[", "t", "]"}], "2"]}], "+", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "\[CapitalOmega]"}], " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}]}], "-", 
     RowBox[{"\[CapitalOmega]", " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}], "+", 
     RowBox[{
      RowBox[{"Cos", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], " ", 
      RowBox[{
       SuperscriptBox["\[Theta]", "\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}]}], ")"}], "2"], "+", 
  SuperscriptBox[
   RowBox[{"(", 
    RowBox[{
     RowBox[{"\[CapitalOmega]", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}]}], "+", 
     RowBox[{"\[CapitalOmega]", " ", 
      RowBox[{"Cos", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}]}], "+", 
     RowBox[{
      RowBox[{"Cos", "[", 
       RowBox[{"\[Theta]", "[", "t", "]"}], "]"}], " ", 
      RowBox[{"Sin", "[", 
       RowBox[{"t", " ", "\[CapitalOmega]"}], "]"}], " ", 
      RowBox[{
       SuperscriptBox["\[Theta]", "\[Prime]",
        MultilineFunction->None], "[", "t", "]"}]}]}], ")"}], 
   "2"]}]], "Output",
 CellChangeTimes->{{3.779816898066929*^9, 3.779816933448492*^9}, 
   3.779817322650556*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Simplify", "[", "%81", "]"}]], "Input",
 CellChangeTimes->{{3.779816937087658*^9, 3.7798169413160257`*^9}, {
  3.779817327350254*^9, 3.779817327399289*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SuperscriptBox["\[CapitalOmega]", "2"], " ", 
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"Cos", "[", 
       FractionBox[
        RowBox[{"\[Theta]", "[", "t", "]"}], "2"], "]"}], "+", 
      RowBox[{"Sin", "[", 
       FractionBox[
        RowBox[{"\[Theta]", "[", "t", "]"}], "2"], "]"}]}], ")"}], "4"]}], 
  "+", 
  SuperscriptBox[
   RowBox[{
    SuperscriptBox["\[Theta]", "\[Prime]",
     MultilineFunction->None], "[", "t", "]"}], "2"]}]], "Output",
 CellChangeTimes->{3.7798169432613792`*^9, 3.7798173317195873`*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{
  RowBox[{"(*", " ", 
   RowBox[{"Ueff", " ", "=", " ", 
    RowBox[{
     RowBox[{"M", " ", "g", " ", 
      RowBox[{"(", 
       RowBox[{"a", "/", "2"}], ")"}], " ", "Cos", 
      RowBox[{"(", "\[Theta]", ")"}]}], " ", "-", " ", 
     RowBox[{"M", " ", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"a", "/", "2"}], ")"}], "^", "2"}], " ", 
      SuperscriptBox["\[CapitalOmega]", "2"], " ", 
      SuperscriptBox[
       RowBox[{"(", 
        RowBox[{
         RowBox[{"Cos", "[", 
          FractionBox[
           RowBox[{"\[Theta]", "[", "t", "]"}], "2"], "]"}], "+", 
         RowBox[{"Sin", "[", 
          FractionBox[
           RowBox[{"\[Theta]", "[", "t", "]"}], "2"], "]"}]}], ")"}], 
       "4"]}]}]}], " ", "*)"}], "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{"Ueff", "[", 
    RowBox[{"\[Theta]_", ",", "\[CapitalOmega]_"}], " ", "]"}], ":=", 
   RowBox[{
    RowBox[{"Cos", "[", "\[Theta]", "]"}], "-", 
    RowBox[{
     SuperscriptBox["\[CapitalOmega]", "2"], " ", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{
        RowBox[{"Cos", "[", 
         FractionBox["\[Theta]", "2"], "]"}], "+", 
        RowBox[{"Sin", "[", 
         FractionBox["\[Theta]", "2"], "]"}]}], ")"}], "4"]}]}]}]}]], "Input",\

 CellChangeTimes->{{3.7798584059104357`*^9, 3.779858600830422*^9}, 
   3.779858687265229*^9}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Plot", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"Ueff", "[", 
       RowBox[{"x", ",", "0.0"}], "]"}], ",", 
      RowBox[{"Ueff", "[", 
       RowBox[{"x", ",", "0.5"}], "]"}], ",", " ", 
      RowBox[{"Ueff", "[", 
       RowBox[{"x", ",", "1.0"}], "]"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", ",", "0", ",", 
      RowBox[{"2", " ", "Pi"}]}], "}"}]}], "]"}]}]], "Input",
 CellChangeTimes->{{3.779858693852026*^9, 3.779858771344214*^9}}],

Cell[BoxData[
 GraphicsBox[{{}, {}, 
   {RGBColor[0.368417, 0.506779, 0.709798], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJw12Xk8VN//B3BZZwbZzahIm/oILZJKnDdpsWQv2oskpKJoUyLSIiEJlaSy
FSJLIccyIiRKyVZCsszcQ7Zs+d3vH7/5Zx7Pxzl3Hue+z+uce89jFjiesHEW
FBAQCJ4lIPC/byHMqu3U8TfInfnfhyBVRVU1lTsH0G491twp2iNtYRZNESeQ
xAePYwO0Pb4xymXSz6CHIZrDrbTbDd935Eb4IQldi6wS2ilfjAXSC66gq8e/
JsbRdmQtuS2QHoyO5NfWn6I9D4mo2MbfRCuCDbUNaX859evFs4jb6JaaXrUo
7a0NHcq+peHI+uP7GO4/gp5SNs7PCu4gj4fTcedpCzDLXtRm30WD4T3Ny2jv
W6Q9PJZ2D4ncidv+cZqgfP0neguSYlC3u86YB21FB7krpvH3UXsNt1GYdl3I
kMzDiEfI85COwdwpgqI+Z2p0lsejnOOW3NhJgrjtrnVnSh+jPXmf/WVpL5hs
Yj8ueIKuBz5+0jdOkBUjokDn9VOkxtJmmNG+pGB6oCr7GdL/UPT0yV+Cmlfk
Jw6lJSHS4XpVf4wghr6X6bXUZDS4t7360ihBuqbq1LykFDTfacbk9QhBkYdj
dbbEP0dTOhMDcsMEbY85z42OyED//UwgHgMEGdgqZUdtfYm2uEgLeBK6XrtG
RH6Wv0Qlg22mxyiChFzSUrxLM9H80ugwMx5BB/2VB+MKXqEj48OdL34TNBY8
btS7Phs5Sb6t9ewmKDT0S6T262wUmaowqvWLoML7t9ZVZuegV8+zJ8M7CFLK
nfYbTMtDPwpe90+0EvSysKleT+M1WvNJlOvfQtC2spxFV1Nfo8U/Q1sEmgny
qfOomJP0Bl26WP7+91eCPvW1zTaOL0AqBwPSHOoICplf9DAqAqPABDb1roSg
LdX3VxVLFiP5S9buSZigd1cd5HVsitFHs5rPl94SVDFd36TSUoysw2eOyr4h
qLKfe/gPrwTZUSa5NRkEVVWkno+R5qL/RnadDr1PkOkVl32zd3CRbNaRkbZo
ut1gMVyJ4aKiSzXHFkcRVJ3zUMRjYTnS55UZRoURVPMkLAzWvEPSxo2xkkEE
ffDzSfxtX4l2Vb8sNztGkIWe9vW9DyqRTc05KXFXgmpHiXt9eyW6+9JkX4kz
bQ/XVQWu75FGdhlD7gDtPXsLQy9UoQlXdr6pNZ0vXaNPOo9qUFbQviMSawjq
GxOdMTxVh/TcEo61/6HQzKLtXN07dcjvhO79GopC8lZ3rmu+qkNd+1lymX0U
Qsnz5ZWG6Pa6wA7HDgpFOqz7b8CrHn10G1J2q6eQwRtX2zivT0jE1GBULJ1C
4edrkiY8G1DyxfmS1w9RKDFR1mMgvAG1zX4YeHkvhQo/OazuzmxAWSsbXE/a
U6hb/Vdh/WADavp9MWD9dgptaJmuT/b8gvhiYdKe6yjUpbdiaqfnV8QqduT6
SFJo3XS41auT31BJW6FsdyYfbfB1u8cI+4bOK0Z0ib/gI71Jo+/7Mr6hi5qv
Y9QT+chgfMidQX1DFd/vv7KJ5aNNIzuC97k3ob3jCR+2B/CRBV+pSOxIM7K9
ZPljwIqPnNseL9+7pxWtVa8ZHe7hofffHa4ILmpHitl3D1gI89DOmnUKs9a0
ozGtebvlp/pR1xtO0oxxOyo2kD35aagfCdz9VjV1pB1Va8aPre3sR+vMHGTH
UttRgopI3uOSfpSYZ/+4f/VP9DXUWELgYj8KDNuJG4w60Iv7J7UG+X0IjOwm
Ex270LZz+xx03vSi3SdNBnJ29SDv9+9jfTV+o6IF6gHFz3jo0LCIcqlgF7KL
Vn/SdnwAXTh0VWfiVjuaK/J2U9iRP6hapidJanEzCtYuzyq+Noxewam43Ys/
oc1bTQ8mXh1FDb8n3WTKuKj8+rBa4Pm/aDSWuujCf4r6bskX/oieQInNgmd/
O+fhQ2cybw9ETCEz6rLCaetqfHEhd5Vf3j90/kbovYyOL1jr38VRQVcBsN+5
OghHtuGKfQ5DqodnwWhhUvUptQ68Ks6j8eJpQRiIVtjG9P2Fja1Ldud4CcFU
xLStk2EPjrzpvkbojBDUH3ufFGvegzvLFWZbXxCCrwaTf6rse3DABrcS3hUh
uHUmzlHxeA8uWSS3bPFdIdiwN8fcIbYHG4weHonIE4J1rNP+QYM9eN19RtjJ
KSGg0JvAE7G9ePkvy/LlV4XBlQQ+afrSh7+5atx2viEMb/viZcR/9OGrFGPX
o1BhEBs6LKDT04fbx0p4sveEwU8ubYfXRB+OZK2Rn0gShj1H1aMjVPrx1AqO
U+V7YQjZ/J+Zi3M/rj3fLuAsKQI/zmqYWfH78YWZwqo4GRFArbJCt0b68bLA
mMhvCiKwYiOPXzLdjwNu2SwzVxGBGCdrW/nZPKwbz7XUXiECFtHWynu0eDi+
PDlulrUI9I3kvz7uwcNe0p4b4yJFwNvizJe4XzzcF1p18na0CIRFJ506xONh
R8nFzy4/EIHc9GZx5SEetmY1Sjo9FYHntcWL/AT4eKWw/o+l2SIw7+wqLjWH
j6kxRkDmZxFYdfeo43kLPnb7EV/BlRWFaedi43Uv+bhj38RkjqIoZC1LUWLn
8vHuVtuVSXNEITUlJo8q4GOTJtGY6wtFIW2we9u1Cj5e9vmYm8VqUdi0vCfm
8Hc+7n63TvKbtSj0bxwrLmZR2Cm9zqY/TBQ2l8S0TO+j8H5ORcCqu6KQrbG1
ZJ8ThXcHvM06EyMKKHn/RPZRClvtTJUVThCFfN2cgyanKLzx35VP816JwllX
/mfmNQrLW+jaWHwRhc/vFCV6Migs9VozILJJFEwedPoUZlOYtXBxVnObKCw/
16587Q2FBUalZV26RaFbbmiOSBmFeQ/76v3GRKEsrvz53S8U5vIfWmcqiUFv
fqqg9DiFsX2k/5iyGJyxyAy3n6ZwfsmNTP2FYqBoobUtahbBL++ekalSFwNN
ixy1cSbBD/St6zv0xEBHLOWa3RyCvW8JW8vvF4PseqHvB9cTrKbpbnUmQQwi
7LBR5UmCeWHf+04nisH6c29U3p4iOGvYOsgrVQwcmzWXpvoQrF+4Lt8jSwwU
3j7MP+ZLsJ2Z6OLDpWLgLpvVeCeYYH/Xx2NWHWIQsqXB2vUBwVs+yEdYdItB
01mHhPY4giVWXdMw7xODOyRW3uoxwdF/jx/a+kcMEtkDe9iJBKcHb6zWF2SA
u/AB560ZBLc+a4xTX8iAmtGWXxrFBCcwzdYvU2NAIXyVVCsl+KhH0ecl6gyw
Eqy0YHMJHlrzjLFgNQPUR58qdVQQzOJ6ebGNGHBTet07zkeC65Z2SyhsYYAP
93PypzqCo27uSpI1ZYCXZnF84CeCF9hCq6QNAyLU2mrrvhC8rkNyi7AjAx6Z
NHBYrQT/2+zfPusIA6pPRsYGthHMTRk+P+PKgGUPfbXGvhNs6dmSMeFJj0dt
503uT4KPCKQoDfrT7S6RUgK/CdY4PC+bCmJA7A4+R6+H4MGK2xa86wzY/b1s
1fFegn1v+1z5Hc6AvVL5YYX9BN9RMea3PWYA/tA/OU4Izq3pPVPxjAGi58Ie
DAwQ3Hz+tmBmCgPWhCTH/BgkWLWxmR2YyQC1+VbJiUMEbwq6nOCRQ9eryuvN
pWF6PNpqmvZvGDCqs0LNcoTgF7c9jdRLGGD3NkOmaZTgj/rsD3LlDLB9qhAW
MUbwn/5C++lKBpS1O7ga/yV4/TaGR10dA0QkPi4PHyd472ja2JsGBiw+kiWv
MUHw5ae2AU++McCJ99ke034n8OieTzsDfnhu4dVOEtybbrzwYBcDJC/t1Deb
ovOwr++FSQ8DzOsW9mDaK8XDdLV5DPjqf5unMU2w7Rud0nkDDDC8HGAaQdvH
pcVcdJgBAbc6Zw3QjlXwbyRjDPiLHshu+Ufw2zI1x6ZJBrgJRvtG0m73rOGV
zjBARp6LWmgLqXqdeSHEhIfXpPbOmaHzX8sWjBJjgvY293pr2qa+b0P8xJlw
XqrqkT9tD3UntqsUE+6eUK1IoR32jZFgI8eE5bWHt1bRzr6arrGRzQR359uq
nbQb19jlLZnLBBHjWJth2hMd44ZS85mwbrt/2zRt5fBHNX8XMuHdOqMS+vyA
AW2271BjgsCd1ulx2of5fT+r1ZlQVGh9h0c7+H7YsRwtJjj1P/VvpJ1qsnYs
bjUTmpe1VObTrh1r8b+2lgn3bCePRtEefOYv4bWBCREqsx3daMvbLb23x4AJ
pjIq2Wtp6wp+WLDZiAkFwboHJul67H7p9UJrCxOcs446vaZ9cT9Hl2PKBIOo
kmIP2vESRSWzLJiQK2F3Zi7tsnwn835rJmSWaQeX0PXvPspsbNjBhEv6PgMH
aGuW2/GS9jGhJTngYxA9n1anJnzCDzFBqPzyZknapxfEz7rgzIRF4yYKt+g8
5F/sV7TwYMIclfxSbzovbcvDH+t6MoHZ+iT+O50vgea1Ggu8mSBl/v0bor1l
bYDhsC8Tmia+O/TT+XTtWlrTdpkJOsmj8Tq0QyI+7KwIZEJJ1yzjs3SeP1Oc
Y7EhTNgqknq/m87/waSMKHhAz59icpQLvV5EDf/M84tnAvfGwXcH6PWU1rzm
ydunTDh3rOyvBb3eJiTzMzakMQEtKDoiyyc48jS3ck0RE+bHhZzUpNfvhtli
lqdKmRAlG1LU1k3wzySTL5nvmKDK6pQJ+kWwVkvtT62PdL4UVapedxBcadg0
saydCcFLhUOy6f3keMtcf5cuJshd65VSbiFYwXu/WGIPE7KQ9wPfJoIdkzvl
Fg0yYYmifb3qV4KnZlMayoIsYNvk5E3X0uulVfCAzGIW2C5M6WNigr96b/5l
uYwFj/NcdToL6fmUuuYeqsGCPSE24a/yCa4ykjwrrkP7uoLf+lw6bynscJEt
LEh9tN64+QW9H/tolI27sGDXg2nxk9EEG0ifMFl3jAUHw6Ne598l+FdK5kef
kyx4uHWf/1QEwavb1rYOnWXB5tm33JxvEVyzyXCEf50FN5MzM2v86fmT2bm0
4zkL9FarfBg8SrDnUwXOtwwWPHK0fNntTHCH7hdG7SsWLEq0+vnZkd5P99v1
vSlggUZu5Yp7ewm+/sImLbyaBSq5ypEllgTLbrPUNuxngfixfxqJawhe7LcN
xauLQ9e5bfey/1L4rixjZZSWONxUjajNHqawaGKFashqcRDJcDuRNkDhnpot
gmc3iMPWyLeDwT0UTpuzudzSVBx0ZokqdDRSeG2uodmMK339ebm3y3MobMLf
YL8/RRw0Dhctn+dGYYNj9zalpInDvy43y5LDFF7dP7RiOFMc3vg0Zu07QOE5
vS8YN/LFYan2MuRrRz/Pu1Tyc6rFwaL0dYyVAYXDWgWVJSlxiA10wPbSFP5a
XdVRsFoCbHO/3u1P4+Mq06UfRXUlIKI+pfNCEh/j91cKrPUkILOqIEnoMR8n
V2yM/L1JAtYYycaO3+Hj82Xpm+V2SED2EvO9fuf4WLkgItntjARQx2VFho34
+HDq7uNKhRLwef2Tb7c/8nDuqV5Tq2L69/evqtGp5GGG/tmlwVwJGCsWK/tc
zMMvPt5tH66RAN+TJaVjmTw8PFxnU9cmAa9yrY3b7/DwFbRVN3hGApxq3NV0
dvJwQoO2wIiRJByYn3truqEf//wnEVH3XhJum/+3/npBH/5Xwmkq+iAJBw3z
In9m9eE5QYtV0+olYZ75ikvaqX3YTnxj+vUmSfARMxTiRvfhd2z3KqNeSYgu
GpeO9u7DqSvfC+YwZoNL91bObK0+fNox6FTM1tlATcnKGdLvz6Lvpu0cy2fD
xxNu4YYHe/CMx38zq79KgXukmaf00V/Yimuy+ISbNMjOtJpfMe3A1srbPhr9
loZjuzT473ltuLC7k59zXAZctYpuFGt/xRd6VfYq8mWgQo65vdjkA36yd0xf
0UcWbtgJT935+BqrMk+cEBqVhbw7Iu7/gh4gi1krk46fkoMEVm99wfIy9Ms1
1ObcpBws4/4LZnTUozrxkO2e5+Uh5le6yTmHJrREZWkhQ1gBMoT7Fmdeakfq
vuKrVG4qwNnUpd57RjpRoNztEVWmIvwU1gvNa+9GW+e+1CwLV4QtxHNeSmIv
Mp/30b3kjiJM2vs5ab3sRVbKVAq+qwih58pN0ujzmMN8DbXCGEX4im733avp
RW6LklVyHiuC/Jpzeqp/elHI8nippExF2JDzS3ZSrw990gsbvFGvCD8E76a/
r+pD+/eezLGRYUNGqOFzq4Z+FB6Q3L9Fjg0HY8ILhVv7ETe5fYGeAhu2SDKX
ZdHnR/URq9BFSmwYrZ8rOv6nH43dWuUyrMqGYf3e52tleCgM/+FErWRD9/RC
iZPmPFS60Nu3yZING8x2dNa+5aGRbWlZH6zZILOtpzqVy0PLTvzqKbFlw5nf
Ko1+1TwUWrBjR6o9G3ZuGrJQaOKhPXZrtS4cYENK9XgUf4iHhoPGfsw7wYbs
G2SRzTI+Uus9t+lgKBvMOvqd2m/y0dygy2OXwtggqVIacCCCj6QXBD9/GMGG
hCc6tl+j+WjcIVKuJYoNqxNfzXr+jI9qKtM7dzxiww6VSseBIj7ySu68YvaS
7t/xO7WX8FGRy3bu2k90/22WF26YUeiVkN3ZHQ1sEOou9pmyolDyo90ap7+y
4ci9zZTzTgqFN7pEZjazwXvK22z+IQod3hpwWKOTDTecz0Uu96EQa2me8MJh
Nkw8n6+UEkch+27VzZKKHFiZq6Sh1Uuh1TPO6WIcDjzhDO8x4lNIgvOcLTiH
A6xnZyYtBilUbKLTN6rMgbGxoCDrcQotSzO53b6EA5IHzqVOMgj6e8qr6ZUO
Bz4NPHLqVCPoU0ieUbouBzQTNs29pE7Qi2dTz5PXc+Dm0yhtGS2CDjZevfRQ
nwOd6EOAqg5B7zfcXxS8mQN/35250mlEUOwsrsfuHRy4tbXAdmIvQafnMBvt
7DnQruafOecgQRbaFmC5iwPrM5sctZ0IEnT+Jmu8jwM7NPTfbnclyL2Sl6fp
zIFVGVIb1bwJ2nhbQXCWNwfKGSqlzjcJUkze7T7pw4ELNaaC4qEEDRQ/ahg5
ywHx6Q83ksMIevrnv6Q+Xw7IXzf1qrlLkMROA/OGQA6YW6Zez3tEUNs8l6ik
SA7EvE62m5NF0M4lG3+1RnEg8NhYxpVsguo0ZdbIxnDgbcbDQ525BJUaFNT7
PuTAZiGp8qACgpIPSknaJHLgYsHU2Z1cgha4du0JTubAG3X3U17v6Pv3fJNa
mMqBy6z8osBKgm4FOG1bmsGBl/e8N4fUEOT1NO/KVB4H/iSfFFzaQFD/i5BP
q/I5ADnNXiNfCDqcc2iBSyEH+CNuK/IbCbJ/J47rizmgpdJ0Q62FrkfPgYnE
93S9/4YW3vhJUO7AGpPWag50db32E+8kaMU4M1qmlgNVYhI3A7oIWsjK1vH9
xIGRVy5nbX8TJKbB8LRu4cDnZq2EyzyCLq9pw1fbOND3tyL2FZ+giY1Zswt/
cEC9zfRbK0UQb/veF2pdHGi4k6gqO0jQkZ2rJvd0c+Ccymd1lT8E/dgvahre
w4HBH2FnVYfo/Jx4+XuSR49nzrU2sRGCzM4GrV1F6HowNYd5tLmXdwcdGeTA
AaUN2ytHCcoLF15UP8IBXa3A8kN/CVoZ2+Qp+pcDEkeNu+aPE5SakF6sN8GB
WZqnjBtoP3jlsD/xHwc8j22qnD9JkEKhZlqLgBJ0tPn159IO5QpOSQspgV/l
QUvjKYIYHxpNt4goAU++YKiCtv+XFzEXxJTA4emRb4bT9P22+fe8ZCpBhJXB
dCbt0907dbvFlUCCN9+R848gPrX86tzZStBnyBP1pn1kTOCLlbQSsBSD+yto
t898WXRVlu6/pFFKZoagXYznXgXySrBn8btTVrQ/SV8uGVBUAp1SYF+lba60
Q1pNSQnSn62dyKRdvkD9wJ65SpB9PHZeA20D9Zm0MGUlmJN76BL9fo9er26Y
Kp9Pj08yaP4k7VV6KWaTC5Tg//9f+D9gt2An
     "]]}, 
   {RGBColor[0.880722, 0.611041, 0.142051], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV1nk4VV0XAPA7GRIZ4p4rGqQo0ZyhsFflbRRSJEXKUEkZUgqpiJBMSZSh
QYbImyLC20aGSGUMGZLx3kuGcG4Zv/P9dZ/fc87dZ6+1195rK51yNrVn0Gi0
BDqN9v9fJhb70r3lpv64VlT73Fw/WsZeprLk3gmkzDtyUu5PP5poDzdqiXRG
gxJh2ktH+lHn9squt5HXEdNvLU+ysx81XuzNeB4ZhoJ7jO/Y435UEzImHR+Z
iLo0U5O7vfrRgVjP0pjIf9HxJwvntg72If1D8tnRu18hxYnDq3509aHGoxNC
P8teoeVk4JBHSx9inn6ZdqkkCwXUux6/Xd6HbG4uHk0oeIOuOi0WW/K4D8m/
nbk++jIX/YZvizUO9qGQpe/joyMxWjAZWdv3ohft+vRoQ5FEEfJ5F0xuS+xF
5QEWsltMi5C5/Rm1W/d6UcVMbcuS1iKU1TP69ZdXL/o4UGr3e7AYWQSk3FEx
7EVVFS88Y6VK0SIfh7QN/B70+frl5P4jH9G49Y9lXxV7EF8gPLf9Yg1KfzbN
32nVheaUD5Rq3atBJpCUf824C8ma3AvSeFODlj7IOPlyexdCqUtl5cdq0OWo
uL1DK7tQlIX26hG3WmSSuFNc8Osn0n939lCCWx1KXvK4YpX3TxThWZ0y6dqA
cmex+4aITqQ9E2HyxqUZqdWlLZTTb0eVHRZ+DOVOFJBLIL1vDQh2HJ5KPtWD
vDWdJDtulCFLl70jOUe56IZ9uovgij96r6TmW/R8EJlEV858P1WGD8eoPWu/
MIK4rit3n5ttxApC/+0Md/iNrjixbs7f1Ilvbyp7XRQ4jsT3haTqP+nF/+ze
Z5McQKLjfcNtCrZ8XBY0rnLL8w+6m7nw6MEXQ5h/V7bwR8wk+lQdvETffhSH
023nKh5PovvuamtsnUax5qWsHa9SJ9GY1q9pv4uj2Pe4UeWNvEn0SGOWl3dz
FMuvCfym1DyJ7hqUFQ/Hj+I9H6dGbIkpJGORvCa8cRQnM7pXcu9PIVK8JSBy
+2980iMrbCRyGg1GsA9Yio/hBSU6Vb6PptHLeklHrswYzhcvYcklTSORG1ma
rvJjWOZp3VWdnGkkWhD23U1lDJdWj9n6Nk2j7bltazVhDK9arqktqziDFh16
t7LQbQwPV+d3aj2fQT0n1Fve14/hOGKnQtXLGeRU/GH+k5YxvOfUJ7Pjb2eQ
QahZ7bUfY/ixoLXqRsUMimvPkFEdGMOmy2eyK3kzqPvuf8e0GeP4rQcKOrZ2
FhVn+/mYrRvH15aXbrieO4uMvKwcLvuP4x56liEPzyJDnWP3I4LHseHP+NOH
Ps6igpvOnilh43jRY4941ZZZlHHig/2H2HGct3jNvJrJWbR2TCK/4OU4HudE
di7Tn0NsHQ21zIZx7CRpE1ZSOoe89MUNYpdM4IYhwxfqn+fQcSWnbjHlCbzt
i05ZdOMcqjjltcVDdQKL3V045dg3h0rfjo2i9RP4sL/tm3ksGhzcMP4lYvsE
/s0glr5eSQMXdV0tLdsJvHb2Gsk4S4N3Dxy3BT+dwBmnXS8MO9Hgs1ayi3Ty
BF5Ta9fX6kKDK3ewZFTaBF6VtP9b9hUaPH3bGHLn1QRW2if/1iGQBuuDGSE7
309g2ejsS1UpNFiuLjvS1DyBJzUGxiP6aCBqaKV3X4zEVx50OPnwaSA1ecI1
WILEAlpdj+MQDaob5RZ7SZF4vD6vYSdJA9ugz+1GbBL/uhqQPSFEh85na2WL
lEjcWabkfnQlHWhlNs2VWiSusLIYW2ZHh/Qv7zddtyHxx9dch1Nn6KA5U6HP
sSVxlcjV78+c6FCwxVYiw57EX7Jii1Uu0SHSaM3fD44kbhBqDVMPoINCh6dK
jjuJu15aqWun0uHdj8/58bdJ3MMYSryaQYfCtWyP7iAS9x7xWVjwig6ZVodE
VoSQmEtPnNR7RwebnRKp98NJPGzW+XFnFR0eZtyfty2WxNMzp+yNB+nwfb2q
YVgaiWdNx5rDR+ggGr3xq286iedS/AzrxumgW/dQ2/UliRmmSZvMZuiQxzzx
QT+LxPOSe+nHFjCAU6nWGpdHYrbRmQSH9Qz4qOSu4VBOYs6zP9KpmxnQpCyc
IfqRxIv+BPrztBnwZ4/eyuRKEi9+mubktJ0BvcoD4nXVJF5B8re6mTLg13jj
t191JN6QcL7pmjsDSpi/AzQ7qPkNeuZcvsIAd8+IfrcfVP62Bd5z9mZAYbfP
1ItOEp9qeWp88hYDApP52xd0kzhKtrnCIIr6/2DL2Sf9JLax7X2uF8MAbmC2
fwGXxOqvf/tpxjHAonOvdS2PxGXGErAqiQGkquu+kQESk8E78uZnMyBgc+D6
qWESl7QYR7PyGGCXK3x4bITEoaus3GcKGHB0dEKvf5TEquUe64c/MGCRL9ux
ZIzEFvSXqXX11Pts9V27BFR8JvkBn5oYsO/5OFb8Q+KRhAq70lYGWMetFhum
HKTbtextNwOygvmytydJnO9BxMaOMaCG5aPqP0PigPIVHpECBvSpWuzZOkti
U7mNZnemGBAV3bZzgPLAa0Ppa0wm5K9h1wBNgBWHbgbZLGTCDc6Pk/kMAebq
hp0+SjDBKeqBtxFTgHPuxP1jqsCEZ/zL3h2UjVbn0g2UmdC4c3L9b5YAX7cb
vKq6iQlfXTwiucICbPjm75FlWky4zyidOiYiwByGiKb8Nia4Lxk+/Inyq0Sl
32I7mWB8/0BbvKgAew+trWHuZoKhg44EfZ4A79XTzZzex4Tvae6bbCh3fTd3
HDJlgkfRIXsZMQHOXG23p9+cCQYf3l60p+x5xVWl05IJoVH93tmUd1X4sFqs
mcBq3u07R3khO6Sr9hQTXs3p+O+aL8CddrFFVQ5MUNqndDuIcsab5IQPjkx4
XXQy6CNlg4PFljluTCjZ4Ru1VVyAo+6c28z0oOYzT/2tK+XuMrkFB72YQL59
pp5EeSOtqD/hOhNE9775W0vZd6tj8aAfEzwrVi6dolznLvtoayATLqCKhKUS
Arz83/fugSFMWF9s54kou/HOGH0LZ8IVq+o3lpSLlReuWnGfCTXLPxu6Upay
/o/uFsuEoUVau/0o28ScbsXxTJjd3ZoUTvlVnXSOxFMmSDy9czqWMk2iMPRY
MhMsZRUi4imb7HY4k/aCCcmeVsr/d+JNqR2CTCYkJagui6E8VJCv8M8bJuhu
MQ4KpaxP2k1E5lLxtb6wuUE5dL3k184CJuxiy6Sep9zu+C51bRETBvdZnTSn
rP7c1te7lAkWxfahWyl7/5A4XvWRCRMJQhsWUa6Wz9vC+cyEkB5i/wSVD8XD
pyQdapnw9NyVjk+UnULFeW8amfC5m/krnnLBx7cl9O9MkKt5cOUcZTHmyTjj
DiYcvCXjt5mypd78y/FdTPBefmjBX2q9XnjkGA/0MUE+RkfxHeXJrBOrdQaY
EP0oOdOd8r7Beczbw0yISfOqWEM5ViW7rWGMCY6MR2c6qPrQfiQa7jJNxXPk
n/2alAMbX599T2NBUlhMeCtVf02SVjvFhVgw6Ftu70358q0sMkWCBeNT+ype
UfVb/v5YzYQ0C2JcnI8YUGb/FXqxk80C66ti1+upes85b2n1YwkLvB7tjuuh
9gcrlaWlocyCcuZ/9x0oH+7KlPJSZUFPjPHaHiEBHjdnlrLXs6CNf9Wgjtpf
GyFDzWg7C25UlW71pPanr6c5K+4fFgzn0Wxb6AJcm03r4O1lQXtaqc5myi6r
zSL8TVnAaE2abJsjcabMrKDQlgVfFqnQpqap89EwtVbsDAs4dTN1WpQP3DZN
t3Biwat7A7YuUyQenEq2HndnAS20paLhL4nX9BqXrQlggcpo1RdHksTNZ9XD
7INZoNi3aTZogjpvhkSPJoayIGSz/PLn41R/ExQPyjxgQVFKldHX39T5KrZZ
djKFBUf+/eM8NkT1k3Uc24+VLLilLeru3UfitOxxdcYXFqxQ1Pp0rJfE5jq1
5LY6FuSf+bxZq4fEr3YEB//7nQVhq7N3df0ksb3ZdFb0AAuEhaQfzmun+qFn
J81eQgi+bPqalED1A6+5wqoEaSEwfEglpZbEq27FRjXLCUFHoFFU+1cS+941
XWW4RAg8af4HJD6TWOtxqfGmdUIwv8P3KKog8eOy1AT6QSHQsq73Gc4nsZuU
q25ClBD8a2+teTeRxPzQKpewGCGoSjvX1RRP9ROJFc9vxAmB6c4J16VxJD4o
1iRhmyQEUgEx25/HkHg9S++HarYQlKs0fL4TQeIhgahvVr0QRO3SGi7zJbHj
j8cVpTLCkPQ9aezPKap/W01O5bCFIdb2I62bug9Yth1an7JIGCpc2pWqrEm8
t0U4Nmi5MMxua3keZknFV+/kaLRRGF5bjHYIDpK4r1xbovmgMDCiTG31gcS2
mTWmA+HCUAQFCzIXkdiaU+G74b4wFK7MLBblUOP7/vfaI1YYosd4ujZyJDYx
fyHDeioML5vi7glR9xndWb86xTfCUL3/wsRqIRLLGmmZGjUKg+eA64rWoQlc
+iv+YJa8COxJRk0DeAKraJwz8XgqApJp1cqrj07gwfAOvnuyCJwTnHkzYzaB
X48f9Hd7IQK07VbfvppOYL1C7fzzr0VAMS+u1cmQuu/tF15hVyICu15eFA1G
E/jm2ScCky4R2LilXMp05QRue96UoLZcFM4wVFuVhsfxvSUGv9qfiMK3s+Ar
5D2ObVL+jYa4eSDbl26v4juGadLmql3pYoBXvJY8a/ob7/219Yh12nzwH8z6
bKkwiu1eWF6QLxQHMWWacffxYfxzVjyyplIC+Av6Z3osB7Fw+czhU2ULIHbH
upCN1jw8d3713MZvkmBXI7yYOdyLTUr3rnB2lILcu85HroZ04YOL93zd0S8F
p+pCtcaUO3BhX/evnAvSsNjDzSlk5zfsxVtynP1LGhJd2kxvGX/Gz44L9NiX
ZYAx6zY88jUPL5vn7MwkZcBazapnyj8OGdHXp1y4uBC6uproWWofUO/ZUNOr
Uwvh8X4YCmytRTXzQw64esqCzPvcekKvBa1colooypIDr13X7pQc7kRq3vM3
LLkjB2d8Z5Ty73WjwODFuuy7ctBS/ZI+ntCNemPW7V4QJgf7Q72+q7zoRo9z
DlnNRsoBP/LMSY+ibiQ3/Cio46EczBIP1D4MdiPaKfWuhBdycNY0vINm0IOa
dh+4t7RKDr7+aMwaGuxBm81PJBDVctCROLxMT9CDIu1c0yS/yIFGqkWrH70X
Gd68j+dq5UCq7r3ipFwvKslvH/jRIgerrEp0g/R7UabGBYPHPDnw0bEy0gjt
RbcWhk0sm8eGcY/SydOqfejZA2efnfPZsGDHFbrPhj5UomAi4iDBhhULfTcF
b+tDtBXS8unSbNBvC5+7ZdyHvDff092yiA3l7yV9mR596JLZA7+9a6j3768M
XVLahxweJEq7HWDDrpjXZo/N+5G/wo2HUcZsiN9r7eB8oh8lJdoo5x5kQ1vs
+uZNZ/pRV4rSlmkzNgjl95fHXe1H1nnPLAKs2eC7tExl6FE/Mm9JSXzozIYB
NfwyqKMf7VZ4pfEhgg1v4rMjPI9xkaHi13PF99gwGCa/wekUF5ksHkrD99nA
vxs8cOgsF1ksVVcpjGXDUZ1jiUIeXOSonLok5wkbCvZP+41GcFHImseSKVls
yBr1N+8v46JwdXzg+Rs22K46fUe7movua3TceZbDhhxiXtT1Oi5KWKco+vgd
G/LVNCWHO7jo300xtNhiNqisD9t8SMBFddvCR4Nr2fAt5c9NdRUe+qb777qg
ejb8GBKMNq/hoVa9L+dvN7LhY9jkqOcGHupB4ny/FjbcLo6LSNTloYmdgV3e
P6l4RNa1O5ryEMfwRv2FUTZ0s8WVhL15yPq4S46pNAE96IDI5goeivBNHdi1
kIAsTT/pS594qDS1U2mbHAFS3LK49K88pDZhEqosT0Dq57ZVf5t4SHB3w+nx
ZQQkeSm+k+PyUDj+zYleT8Ciz0aNB4X56EOvmnHwRgLs5RVO8ubxETnf1t9n
MwERj5x1Lkvw0XGL+lEHbQK4Hxd/cJblo9Wjb6q0gAC7qPPdP5X4qGT5Je8W
YwLO2Xhm4K18NLHn5evPBwnwKVAIf6HHR6uce7nFhwjgrGwNCwY+Ci0wM3tx
hACtZI3m1bv46NhhzbVeJwhYKmyv22tCPfe8YOd8koCiSMWsK4eo8R8nP7S1
JYDubraGbk6N94stYniagL/njFeOWvLRuL/gh6IzAZ5RHt2GdnykmrGOLe1K
wPbna7UDHfjIsu60odBFAupGfnrnneGjoiXNeb8uE9Dc/OWTwImP7ubmRbz3
oeb7JVdr1J163j5S8foGAet8VEvrL1PjM1fPJvsSoGOwRj39CjWeSaxjWAAB
+vlS/trefKTCu7rTJpQAh2967M++fKTgf0PgE07Ab7qBosctPpJSup0eH0lA
yf3iXwsD+OivRdTC1mgCMrbu7l8ZxEdD4w8r/sYQ8MDddd6jYD7qCn/ixXlE
wFPnIwOMED6q/pjZbZZIgEH1z64XoXxUbJfzwP0JAfP5rb/5YXz0lla4/94z
Ai7v0ctdHMFHidqVb2pSqPllrjI9do+Pohq+nh5OI0DWQ1fZIYqPAl2+KSzI
IOCi2Kd42/t85Jba7bf/FQHLFbNvaT7go9MGfC3H1wQ8rKkemB9DrX/nyEBg
NgEFJW2j9ZRNvAWJKW8JOHn3UFRoLB/9w5k9VJ5HwKRjSs22h3y0NZsl2ptP
QLu2WVIr5bUm8wuZ/xEQ7dkpfuERHykPSrssx1Q+repFxihzAjkrthcTkJD/
PtIxjo8kVixtPvGBgEzFrekNlBlFK0N8ygiIKuo22hBP1ecxdYivIGAm1sbr
BuUBwcbxgkoqP+bH15ZQ7rynk/r9EwFPTgTZk5Qb1sHxv58JMO58vGRxAh9V
ftolxakhIL3s1BFNyu9PHyjVrCPA+sN9iR2U3zAPXzFrIMDGXAQB5dRES3X3
bwQofLzDW085btvJzshmKn8bZsVkKUc0nY7K+k5A5TV4wqO+F3Dxwp6aNgKE
L2x7kUXZU/LS9FDH//dTh7ITZed0r1cSPwnY7bVMXJ6y3W5fO/Vuav1sZm3y
qHiPdgdy9vdS+b7uvWwfZaPrYdVn+6l4R5/v+ULla4dC9I1AHgHbckOaDChr
5cZtThkg4NK7vZ9eUvlWP/SMW/aLgNtfp5XmUV42nBbXM0zAL1zdYk6tl5hq
Lmv5OAEfRG/aV1LrPVfyXx6QBITkNG4Ziqbq3brU6cQfAn7yVW2EKLdH1zbE
TVP1eOZ3gRhVP3UbmwMLZgn4NCLD/xPJR+VfOnS/0ziwK+DmuRaq/l4JDyYR
QhwQS+w76UrV5/Onvy00RTgQ9PS/+lVU/T7U/ytuNo8Dg3nPIuuo+r51WfhS
pAQHxr6lj05T++FI37J/JNgcEKc/Fn/px0cb5+wzRTgcuHJpwkuW2l/inHSC
sYgDaw0lx51vUPt17xY+uZgDA+fcJhnXqPPh5d6wzpUcKH5tbhB7iaqf8tA/
31U50HtvXsfji3zU1lF/snE1Bx40112Jc+WjcCnrzVUaHLDQ1Ml0Ps9Hfy66
tbzZwoHTTd70AOo8qQvJ3ZGpxYGHaj0hGqf4KOP5dHqqDgdcuqUXVp7gI5um
AJ94PQ5IEHSJNuo8qtz6SPn2Pxzg/1V97kqdZw/ppectzThUP7jHtdThI/dF
85oOH+HAKz/Hlu2a1PpuMgLjoxxIWHO3UHETNV/7ZhkDKw7IbD5mkanOR+c+
DuZq2HPgW1R9OyzlI90wOQb9EvV81HVkOYOP2KmW56Yuc+D7zVHunlkeGilK
bJi4woHAY601pyZ5KOn36hS+NwfYQlMX3H7zkLi5vmHDLSq/Xsw+WhcPtSue
jk6J4sB5p5SO7+95yHylbm9bNAfmXMLHct7xUI2G9GaZWA7Yt25h+mbzUIl+
Qa13PPU9s0rZX2k8lGojKWGazIFl50KkJaN4yC0p1286lwMLy31k/Ox5aCAj
pG5DPgdKF3coXzjBQ3Y5J5VOF3IgMtVYd99RHjpSPh/XFnFgpO5tQKMhD+ly
T0wmV3IgRif1ifYmHhJRF3U92MqBUC/Wgo0zXHRjczsOaOeAceniFWtJLprU
fb2g8AcHjgaEmCsMc9HggeMZKj0cqAwbZ1d2Uv3Z+VX/1CAH4kXSt7h94KK4
NxbWybMc6Hj4x3/an4vkCjVettLkwVigrD3fh4tCSxnTUkx5iNFvWzH/Mhfd
bMyI9RKRB5dv+W877LnIQUBrNJGShxX6MVMNBlzUOdeoHCAjD9oWh4vP63LR
UdF0twJZeQgOkG0QbKLuK/JmUiry8qAYFcD/sZyLypTUThxTkIebvtvH9BZx
kb7a3MvwxfKw8KnMobvSXJS3sWG6bKk8WFZHiX8R5aIN29L2TynJw9qKc8tp
NC76H2AmuG4=
     "]]}, 
   {RGBColor[0.560181, 0.691569, 0.194885], AbsoluteThickness[1.6], Opacity[
    1.], LineBox[CompressedData["
1:eJwV1Xk8FP8fB/AlqVw7YmdXKIqSdBNF3p/KUXIlSnIlknLfyV2EXEmklL4S
EiJEKjuhu0RFOcNuUrlyW7vrN7+/9vF8zOx8Zt6f1/v9UXTysnARpFAoaQIU
yv9/FzFFmlgaUboUCvrD0rhSp4ArrF151QEo0eLM97e9mFPdqabtaV5AeZoZ
u7Uogtm7523/47QIoBisTbR9kcJs9ftZfC8tBXo36yS/Gs1hNidOSN5Ky4HT
BpwFxtoypklWSOP1tIegFnzaKCiKYCauqruVkcaEQv7UEr/tLcw/M8ILe/ya
YXHZZNaVkyymFu+KeYX3dxid7MfiskaYb3usLwiu6YWbbfvNtbZMMdFey/l8
JzaUrmjjOrbOM228D4xVHRuE6PuWGedEBYg6RdVo4t4QdHftsHpjI0RYXle9
2+05Blh09cFytyWE7OLn+1JPjYOS37zDFYoI8VX53GIZr3HQLrbRXSkqQiQb
aLz+L2gcdoh8+ZpHEyEE40qMKuLG4RP7oFSOqggxKHL7UFvROHgV8DUNLUWI
x1iEg9zoOMTKrNY5kS9CHJLbc74waAIy7hrwr+mLEpe2v3xExE2CefTM3V5P
MaLN8PyBmtRJKBUp/isbJEYo2W7tfXh9EjoDj94yjxAj6i/eEs8pnATaF2xL
fooYwW8NOB32ZhI2rzhVKlAmRgQGr125a+kUKEuLzASOihGnnl+Kr7g0Bbq9
zu173cSJqpbdCg9SpsCqWrUwz1ucEBqYeJybOQWjgX3+C0HiRK6EI/tKwRQI
zb6Yz4kVJ3octMD79RQM6RS6Jd4VJ44I/p5UWzINoOmyDusSJ/QNjRzzY6fh
q+KCwkNDCcJszPlbQPI00IxyPEVMJYhjWRGm+hnTkLFB4769pQTh8bdSh31v
Gga9ajv/OUoQ11JWyax+OQ0rV7tM1p+TIH5+m2y5tWgGxi+umxQvkiBiTufs
uxY5A86LGZjWYiqRIllb6xw3A3WvtcuKRahEVu3XreqpM/BosfWcDEYlSsRE
FL7kzEBN+qqInhVUorXMn4sxZyDc8sxa6hYqoTx3oCqRNwN+P87Zix+jEi/j
J9deDJmF5m7ONt97VKJt2YFrvOhZ+PzS7btNEZUYiL+1KOjyLAg6nr696yGV
WJpg0OuWPQsjSikxP2qohFHC9Syzulmw6N2VXveeSjQl6IjJCs7B5npbl+ZR
KtEjciUkXWQO2Hkr7FQnqcRows9Bcak5WBy53v78LJXALie/pCjNAVp/VzuK
ghGHL/eGD+jPgZyyjO/npRjx/fLFf+Xxc6B9OaM9m4ERg6LtDhvS5uA6X6sr
agVGzF3e2JR3Yw608pYdd5LDiBWJbUWZD+ZACmLe0xQwwjZRxTns4xyYHWh0
N1iHEX2JH9r2S3Jg11TJiSZ1jBgXU9Svl+GA7HPJ+TM7MEIwKaBCezUHwjab
iy7SwgilpJWpm7ZzoM4zMEVZGyNOJXkfkLbiwBr+WwX1PRjxJ0n62Y/rHEgs
+qMXZIwRqQInF17f4YB/pl12pQlG7Ago31tWyAErt1n+sClGRNuavo2s4UCI
Y+JPy0MYIbMhrk3xOweM+yhaE1YYUXe7TUaklwMx2QGD2FGMcF6ubDf+iwN7
V8vnqVpjRNncC1b9DAcOz+zYbWGDEfvfzI+dpM9DxnDIrI89RoxoG6kbr5qH
m1dthLwdMCL94fUg9XXzEKdOlTvjiBG9GRp8Ic158NAL8LJwwoggFy/R/CPz
IGS4vmjCBSPyBVnKg9fmwUY2LiPYHSOMA7e6Nd+ah6xRCz9ND7JevyOKa+7N
Q7csx36ctG6L3Pb4qnnwFf130tYLI1pzjiDV1nl4opmoJuCLESFS9y4s754H
ax/1uDukFS5NvOKw5+E/b7kpbT+McPdMNXk/OQ+fPZcJufpjhNDudzbu0lxw
NzhNSQ7EiKIyxm1LOS5sOk4dEw/CCHMl1z4dJS44ew9PxJO+KSp0WlydCxMd
UzsDgzECRVg8mNLmQv+ludAB0gPjd0a693FhF2ug1eIcRmzr2B1QepgLtj5G
71eFkPkySazJOM4FJZ3i05Gkw190zIef5MIUq0m+m/S7wqBoMz/SHIOWhPMY
4S336qXmeS74b7zR3EkaT5VepnCBCzr193+rhGLEiaDylLE0Llw7v/xsNWmJ
+p3vom9yweaJijKfdK1YvRAtjwtyVp3GKIzM01EjKCjmwv7Jnv4w0stzP5/b
WcUF4SN/fz0mXTdkU/n+ORc0LJ7a/iV9VpM1YveKC7rfxfbKhmMEI/rs+rEm
LowN37luQLrxw8TJ6G9ccCrVPuFB2pseelu6lwtN+/OyU0jLOwm15w9y4fBC
3sES0u+KE6V2/uPCH2VRr1ekA2ekTd/PcUFSsWBJJ+nVe2/F2QnyoNTZGP9L
+lOicsOoCA+++rbcnCYd+q2EFyXFg8QOahaPtMrqHVrScjxI2cGWoERgxFf3
Ot98JR5gapv4/78eVW1QorWRB+Fuza4zpDcJfvr1ToMHO+6VGQ+R7jA+utpO
lwe7s6tLukjHZv6wHTXgwSzWcvkN6e39rplRZjz49fbvr4eke9XGWqSsebA3
da4+jXRiULBYviMP6jaNyvuQ1qqnGGq58UDBu3HOiPRPsfiodz48+CPvf1SB
dNpRyWe2ITyQW5jX/EfWVzc3a3okmgcFbbbZz0n/GVLcGnWZB5ucbsTEkM7Q
LDorlc6Da9ZPJ/eTHv1Q26t5jwdpmi2n68n9zabvk31XwoMch36vYNL7nd5b
2T7mQa4nVUCV9J2ZzneRr3lgug1/FUHmx3iv82KpZh7cqF7DXUN6LnEI7n3n
gW59JdFA5s9iNa/y7W8ebN/1e26KzC/fPWb0+DgPLDq+eceRflAtrjrC4YGR
U28ITlrIZGXOcjE+XAyfMlYm8/84COKPb+JD2a+PIhJkPznVv24Y3sGHPC+a
TlgARlDFzfkRwAfOmU0ig2T/nc519Msz58OF3wNRZWR/rvgYZTfsywdz1dRF
4j4Y8Youcj3iPB/0fT3gmDdG+DmlfZa8SD5fLmH1f2T/f5jJNdxxjfTCMGuN
J0aErW7cGlHNh6Mvxs9NncEItkC58W8mH2bzXouuIW3cd8v18Bs+HJrtCzJ2
I9e7E3RrXTsfhmaOs9NcMaJGfsOyZg4fxHLXaoycxIhJRlqvgu4CmP13YvLH
cXK+z4bPJxgswHPT0C/vyXnY8O0sPmW6AJe+f7lbcYzc3wx947cOC/CbYroy
iJynW6Tnqn2iFqBpQ+y9jxbk/KE6ptQ3LsBAY4Fd7X4yryPGRWofF0AlfUr0
lCFGaDftfJnRugARPwcbxQ0wQiRJav7MwAIUTLrbHdqHEZYxJyuWCVGQXMFa
6/u7yfkoSF/1SJmCjkk2TN3aSuabHzYt6EZB8et8LPJxjCh29fEcdacgbfNh
+rQ0RmxocR7o9Kaghm767b1SZP/kHWyrDKYg05GX4y1UjFA0knl8Ko6Cbtfw
fr8lz0PpjMqAdwUURG+1+IfI85Sz8e/klQEKch5/YP2rkkoEZ/a4h/+hoHw/
3Pl5GZWYoXxmnxmhoH2eQceTiqnE5Jear/umKUhqfoucbB6VGD4XWzm1WAAZ
OBXuH0mjEr0vFf2PKQugyMRCr0AvKvHaznpCwVkA3f7ETF6nRCXePBo85XRa
AA3Unm/6tpJKvFtyruOuuwCqzQ/2jZIhz//yrBdrAwRQ+tv3HEKCSnxd3Jmi
FiuA/LPuZfyblSD6S+zUtAoFkHVM22H5DxIEl+fkYjYkgNY5lxrUe0gQW297
fAvzF0TNX7/QT+WIE/yhkKrAYEFUqXlQWCxLnHinHXfVK1QQXZAIPVKSJk44
teeanbgoiJZ/E97UHyNOpEt/f62XLohexMxo0tzFiemEvTWilYLImGVTkqcp
TtQG0bOyJgTR6OBTavx7MULv0AubKt9FyPCocNGZX6LEhp9mLzfECqG6TeNO
vh+WEb6Yj87t9MUo1vyuMSttCXGytNnib6owStrSUNPtuZhYu/GseVDuElT+
OZ72TH4RcXWl3nD3f0uR/VzKJaqQAOFY8DADZS9DEkEV6uEr+UyK5JF1/Q9E
UK6h7OWdahzmgeFdR+3vi6JmWdUp1DTNdC6y8ZR5JoaOeom2e0ZPMPv4YmnN
b8URXxVbG5o9xhR+xbN0eimBpmvDQhdbDzEXPNYvbGujIvOLWgqdF34xzRsP
KHmdwVDBznVBxy1YzEPy+z/t/YUh/d2Pca+3PcxnA6zhKk9JtOtQiuSYbxvz
/O+VtviwJFp76Yhzs+NH5l3bmd144HJ0mlP3dklLDVNhmZfXounlyGrkpPlk
TDaYCmwp8PSTQrln866krmuAn27JFufmpdDSfiFFkTct0CyaaOITIo3CDD/7
TPz5DjLBBg8yQ6URpYIqb7i0HZzYlGV14dIo2y+1MUG5HaaeBTSKXJBGy/V1
U/oc2mGFp4N23mVptBU1MN9+aQeX5m3rv2VLoyDnz7VuNR3ASW8X2s2URnP/
3I0k/LtATyDd6eQLabQ+SvSuckoXJHmYEvEN0qi0cg5bX9QFCgYN59teSyPt
TRuKJ350gf5M8bhXszTqjxmzTjfqhtRjkb25fdJI1GxM7ZdsDyivXPdsqRAN
PaN7hg+X/IDbDzmSysI0VPTabqzt5Q/A9zS57llKQ5nh2jr3u3+AsHOAVIgY
Da0Tvf7vgnAvDNxvOPNXmoZOPNSZt9TohXwNR5kmZRpKqjlY9ii1F1RMsgKv
GtLQ2fut9UO7++BOj/uHhwdoqKcadprv7wOGN1r94SANnc7slMu36IOlV399
FDKnIWGNNX9VXPtg8LuGcqA1De39sKQ3LqUP7jt//mLtRkMRlY+W5nX3gWqo
6NaVl2lIUyRCpc2zH+IS5HXwJBp6pFO4ojWwH35e32wokUJDig7U0Ffh/XCn
6rAdP42GSj03W0cm9wNt9GZ8zw0aylkktcSspB8oTmr9t4toyOjw2EfjwX6w
99YdziimoS3/+FfcR/vhabj5bHIpDcW7bJkMm+6HwJv+EhGPaMje95d9gBAL
hr4+2+VQS0PpGmEnqlax4JuhydVV72hox4oEwvMwC9SPONymf6ChkFp389xj
LEhz9rlPbSLrzRZ9+MqBBcZR15gLLWR9U2fy+s6yoL62+++Pdhr6kHpC1OEC
C0o3eurd+U1Dm393Jrg/ZIGYTqTZ9b80VH/dQ7CgkgVuRmk2qcM0ZJE6dL7l
CQuUXB97R/4j3TKyZ7qBBVl3+NmOczR0qNG/OuwbCy5KpUwpLMPRhhZHdG6e
BXczvcL3ieLIaUr+7xIKG+plzZecEsfRJCe4I1qIDRQlSZkHkjgKWXrdcLc4
G0LVr+porMCR3hfiTr08G27W+L08KoejNoGEqnJFNtTqWJqGrMRR5dmK1ERl
Nszq0RyZq3GUzb5wfPFGNgRYZV44sAFHtmJrkwW02ZDeHiTmvhFHn7w/Kpjo
sqHSzvpa8mYcGVbu413Yw4ZxF5mCL9txJO/VZVVhyAbJP3ObpzVwlGezteKB
ERu2eHbUMLRwdEyzVi7FhA2egTff2engiJag8A0/zIYkzvnDkbo4aunyWPbU
ig3F4bZduQhHfYs+iRhZs+FPrPzILz0cJXMX6arYseFUZo6krwmOvmEX61gu
bIiRjbyRboYjabGLL7pc2ZCX47im+hCOAke6VhBubOgvUNTgWuHIkd+Vru/B
BgE1wbqV1ji69+Np5i9PNiiU9RvsscFRs9bJKj9vNtjX3LWOtccRv6drsZEf
G8J0LvYVOuJo6f1Nsun+bMgmnM+8d8KRosR+7E0AG57q6U8MO+Noe29E289A
NnS8UQ7FXHEkfGiHw3AQG+aMhRdvd8NRlXDrzZ5gNjBaBpKszuJIecunsCfn
2KBp9RoP9sCRW9Z/02EhbDjSXpBzwwtHm4jPMxvOk/thF6fy3AdHywfHfRtI
p/edLv/hhyOhzdnH9ELZUOFyYJdgII4u/ed7q5T059/rG5SCcSRmprRFMIwN
/zxEjA1DcKRq77YISGPjf766heKoW0FomQvpzYHv7RLDcfRc5cU2f9KmnAcD
pZE4Wns8yNedtEd4oldLNI5cFi+tNSGdKOgxO3ERR5/H7OfppB/EmkThl3A0
HOir8p5c/53IJpGd8eT91/ZtdyX9O1ni6vHL5PqCPeJD5PsvlRqVDU8ivxes
i46RVsn8lHcnBUfRD79wHpLfbyhbtrHhCo7mC0P/jZD1MZb7dPbFVRzVaQ5m
KZM2lx+5z7yGo4V37c2GZD0tV4oPPs/EkfGBDZetyHpbr1Jb+ywLR73KzOem
5H7YKhx0rr2JoxuekXbbyf06oXgmt+YWWZ8OJ4dF5H6eWVO4suo/HJWnxdi5
+JD5VHptW3EXR5p4+74ZLzb4Kg/cKL+HI46ev6M/mZfz65TopffJfA4vk9xy
lg2RKvusih+Q/SnpkOJJ5u3ieqerRSU4ijctZ1wn85i44Q61oBxHeMEpxeKT
bEhVY5rcqyDzkOVxOeMEG65t7Ll8twpHf72etJ51YMPtzXJL7zzBUbhkU2+b
DRtyt2gb3H6Ko0dZMYnuZD/kb7W5mP0cR743HEaGyH55uP06JesFjuT8XxSV
mJP9qV6tm9lA5vfuxbFhst9qNNpCr70k76f11TMOsoHQlJ678hZHOpWG7I36
ZF60U/8ltODoScYarqUWG9p0Hm6O/4Kjmycb4wbV2dC5u8njUitZrzUvzU9t
ZQMbxP5caMeRZfqyswqqbJjaF9cf2oejgt2tFUkr2MDRK1A4zyL7P85F2Bdn
A1//lf25nzgaiL+vj5azQXi/UGfAbxwpbKg7eWMZ2Q/GkV88/5HzJyBxud4M
C+RMciQ9JnBkL8KqCxxngaJpndnZKRzlBA6KXh1mgao5973rHDk/Kj84xbFY
oG0Z3OgoQEfzIYsEXZpYYG/rXWUhSUfRh71le3JYcCW68K+BFB3lSVhjr7JY
0FjYq6hNoyPvYC4n8yr5vCnz5DUydEQQq67MxbJgJmmr66QCHVWqP+595M6C
VOY4I2MLHT3e9XnLzHYWNPxUNUvYRkdPquo+hqixYFr0ZEy4Oh3pvRFm/1Ji
ga31l3+ntOjoTvwqh3AaC9b/q3iniejIZsfOhptT/VC/OiC03YyO+Dm/l6SV
98PU/pJHHw/RUSPtcdf9wn5Q8fo5+OIwHXG6qbVFOf2Q/NTKqugoHXU0dfm5
JvXDccsdm847kN9Xla/2wbUfJmNmfsh50dGHcurq3Yx+WPv73D7HZDqqebR9
41XyvJaNiZwJT6Wj3JK3g89t+gBTvPTgVhodrXZ3ef7BpA/mrNOlOjPoSG5W
4HrG9j748KaUZZVDR8vbNp3s4PWCbyHrwsEy8v9vGMvlk3qhztWkccdnOnrt
9jmuRe0HVCyyDLb6Skd/Tas208V+QGGOjZp/Gx35Vj57vOZvD1z55ppe3kFH
iYuQ/pXCHnA2jHZWY5H1z12R9FyhB0TWVQutnqSjXbpG1yyFu+HogIK+OM5A
qMHF9nB+B2xbcCldwmCgP/SJalpUB4gxHtAFVzCQ+oXip/eOdwBxQOPPtDwD
Jad4PdqEdYBKyYGUXmUGena8ew07sB1m/XzbKzQYyBRf97hz13e4IdDoYWPF
QIVvtU3nslvBf8Wyb5ZHGUhP3STX06cVTLebIrNjDJShYXa1XL8VBF2+L9ez
I5+fcebwneGvcPbNUPVGFwaKvawbq6j9FXRSaIICAQw0crhzuvLDZ+iWc80o
SGegvFXJ4z7pn+CIss7PrgwGIuY85rYpf4LmjZLqy7MYyP/cXE5QVRPU6z5t
Cb3FQO47+qiJXz9CoSNV3CKfgeQGh91ZYh/AN6/6Areagb4G5InYnXoDS9SW
+hzqZCBKEM1A5jkBkerdzNhuBprNCqUE6RLA0Xkk8ewHA6lQExMehzNhyMS2
eC2brKf8ZJNXxHP47FX2a36IgfZ73h6LD3gC2RXW9vl8Btqy9cXsNfVyoD3b
WNJJkUFlctFjSKUMkhsFudgiGYRJ/LHkaJRCVGtx1vklMsjxfaCs1fx9ODVD
aTXHZBDF9emzeq//oHehdU3schkUqRn+JCT1Nhxb+sD3qbQMupM7lVO3+wYY
y1hha2XI60PvPTWTrsBLRVWH47IyiDj38GO3fQLoqi6UpMqT1yOOHZi8GQU1
275yX64ibS3U73bXB7Zq3z84r0iuR6FUsDSu6P4PTL3miw==
     "]]}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0, 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  Method->{"DefaultBoundaryStyle" -> Automatic, "ScalingFunctions" -> None},
  PlotRange->
   NCache[{{0, 2 Pi}, {-4.124023659400009, 0.9999999999999918}}, {{
     0, 6.283185307179586}, {-4.124023659400009, 0.9999999999999918}}],
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.779858720208568*^9, 3.77985877206035*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Table", "[", 
   RowBox[{
    RowBox[{"Solve", "[", 
     RowBox[{
      RowBox[{"D", "[", 
       RowBox[{
        RowBox[{"Ueff", "[", 
         RowBox[{"x", ",", "a"}], "]"}], ",", "x"}], "]"}], "\[Equal]", "0"}],
      "]"}], ",", 
    RowBox[{"{", 
     RowBox[{"a", ",", "1.0", ",", "1.0", ",", " ", "0.5"}], "}"}]}], 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.7798587742617188`*^9, 3.779858909370906*^9}, 
   3.779858977260297*^9, {3.7798590139514008`*^9, 3.779859114182328*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{"-", "4.463739029061309`"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{
       RowBox[{"-", "2.1508869578810144`"}], "-", 
       RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
    
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{
       RowBox[{"-", "2.1508869578810144`"}], "+", 
       RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
    
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{"-", "0.6592650159460414`"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", "1.8194462781182772`"}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{"4.132298349298572`", "\[VeryThinSpace]", "-", 
       RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
    
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", 
      RowBox[{"4.132298349298572`", "\[VeryThinSpace]", "+", 
       RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
    
    RowBox[{"{", 
     RowBox[{"x", "\[Rule]", "5.623920291233545`"}], "}"}]}], "}"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.7798587860866823`*^9, 3.779858853461214*^9}, {
   3.779858898546256*^9, 3.779858910049852*^9}, 3.779858978037785*^9, 
   3.779859014637591*^9, {3.779859077210828*^9, 3.779859114665954*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"%104", "[", 
   RowBox[{"[", "1", "]"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.77985891184307*^9, 3.7798589221119537`*^9}, {
  3.779859119800037*^9, 3.779859164880829*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"-", "4.463739029061309`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "2.1508869578810144`"}], "-", 
      RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{
      RowBox[{"-", "2.1508869578810144`"}], "+", 
      RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"-", "0.6592650159460414`"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "1.8194462781182772`"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"4.132298349298572`", "\[VeryThinSpace]", "-", 
      RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", 
     RowBox[{"4.132298349298572`", "\[VeryThinSpace]", "+", 
      RowBox[{"0.8759586158947489`", " ", "\[ImaginaryI]"}]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"x", "\[Rule]", "5.623920291233545`"}], "}"}]}], "}"}]], "Output",\

 CellChangeTimes->{{3.77985913699714*^9, 3.779859165448475*^9}}]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.779859123086986*^9, 3.779859123100409*^9}}]
},
WindowSize->{808, 405},
WindowMargins->{{187, Automatic}, {Automatic, 66}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 1074, 31, 121, "Input"],
Cell[1657, 55, 342, 9, 32, "Output"],
Cell[2002, 66, 342, 9, 32, "Output"],
Cell[2347, 77, 155, 3, 32, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2539, 85, 596, 21, 55, "Input"],
Cell[3138, 108, 1809, 51, 62, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[4984, 164, 180, 3, 32, "Input"],
Cell[5167, 169, 584, 18, 48, "Output"]
}, Open  ]],
Cell[5766, 190, 1359, 40, 87, "Input"],
Cell[CellGroupData[{
Cell[7150, 234, 533, 15, 55, "Input"],
Cell[7686, 251, 26266, 444, 230, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[33989, 700, 546, 15, 55, "Input"],
Cell[34538, 717, 1458, 39, 77, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[36033, 761, 233, 5, 55, "Input"],
Cell[36269, 768, 1238, 32, 77, "Output"]
}, Open  ]],
Cell[37522, 803, 92, 1, 32, InheritFromParent]
}
]
*)

(* End of internal cache information *)
