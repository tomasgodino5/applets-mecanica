(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 10.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     12321,        449]
NotebookOptionsPosition[     10969,        396]
NotebookOutlinePosition[     11304,        411]
CellTagsIndexPosition[     11261,        408]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  RowBox[{"(", 
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"delta", "-", "d"}], ")"}], "^", "2"}], "]"}], "-", "d"}], 
   ")"}], "^", "2"}]], "Input",
 CellChangeTimes->{{3.777060909998047*^9, 3.777060989171974*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Series", "[", 
  RowBox[{
   SuperscriptBox[
    RowBox[{"(", 
     RowBox[{
      RowBox[{"-", "d"}], "+", 
      SqrtBox[
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{
          RowBox[{"-", "d"}], "+", "delta"}], ")"}], "2"]]}], ")"}], "2"], 
   ",", 
   RowBox[{"{", 
    RowBox[{"delta", ",", "0", ",", "2"}], "}"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.777060994509321*^9, 3.777061007033884*^9}}],

Cell[BoxData[
 InterpretationBox[
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"2", " ", 
      SuperscriptBox["d", "2"]}], "-", 
     RowBox[{"2", " ", "d", " ", 
      SqrtBox[
       SuperscriptBox["d", "2"]]}]}], ")"}], "+", 
   RowBox[{
    RowBox[{"(", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "2"}], " ", "d"}], "+", 
      RowBox[{"2", " ", 
       SqrtBox[
        SuperscriptBox["d", "2"]]}]}], ")"}], " ", "delta"}], "+", 
   SuperscriptBox["delta", "2"], "+", 
   InterpretationBox[
    SuperscriptBox[
     RowBox[{"O", "[", "delta", "]"}], "3"],
    SeriesData[$CellContext`delta, 0, {}, 0, 3, 1],
    Editable->False]}],
  SeriesData[$CellContext`delta, 0, {
   2 $CellContext`d^2 - 
    2 $CellContext`d ($CellContext`d^2)^Rational[1, 2], (-2) $CellContext`d + 
    2 ($CellContext`d^2)^Rational[1, 2], 1}, 0, 3, 1],
  Editable->False]], "Output",
 CellChangeTimes->{3.777061007655139*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{"masas", "=", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{"{", 
       RowBox[{"1", ",", "0"}], "}"}], ",", 
      RowBox[{"{", 
       RowBox[{"0", ",", "1"}], "}"}]}], "}"}], "*", "m", "*", 
    RowBox[{"l", "^", "2"}]}]}]}]], "Input",
 CellChangeTimes->{{3.777061359771345*^9, 3.777061385118177*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      SuperscriptBox["l", "2"], " ", "m"}], ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{
      SuperscriptBox["l", "2"], " ", "m"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7770613868407097`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", "%", "]"}]], "Input",
 CellChangeTimes->{{3.777061388553894*^9, 3.777061392048523*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       SuperscriptBox["l", "2"], " ", "m"}], "0"},
     {"0", 
      RowBox[{
       SuperscriptBox["l", "2"], " ", "m"}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.7770613925009327`*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"kases", "=", 
  RowBox[{"{", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"m", " ", "g", " ", "l"}], "+", 
       RowBox[{"k", " ", 
        RowBox[{"l", "^", "2"}]}]}], ",", " ", 
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       RowBox[{"l", "^", "2"}]}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       RowBox[{"l", "^", "2"}]}], ",", 
      RowBox[{
       RowBox[{"m", " ", "g", " ", "l"}], "+", 
       RowBox[{"k", " ", 
        RowBox[{"l", "^", "2"}]}]}]}], "}"}]}], "}"}]}]], "Input",
 CellChangeTimes->{{3.7770613963982058`*^9, 3.777061445365609*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"k", " ", 
       SuperscriptBox["l", "2"]}], "+", 
      RowBox[{"g", " ", "l", " ", "m"}]}], ",", 
     RowBox[{
      RowBox[{"-", "k"}], " ", 
      SuperscriptBox["l", "2"]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "k"}], " ", 
      SuperscriptBox["l", "2"]}], ",", 
     RowBox[{
      RowBox[{"k", " ", 
       SuperscriptBox["l", "2"]}], "+", 
      RowBox[{"g", " ", "l", " ", "m"}]}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.777061449200856*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", "%", "]"}]], "Input",
 CellChangeTimes->{{3.7770614504247*^9, 3.777061453341201*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{"k", " ", 
        SuperscriptBox["l", "2"]}], "+", 
       RowBox[{"g", " ", "l", " ", "m"}]}], 
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       SuperscriptBox["l", "2"]}]},
     {
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       SuperscriptBox["l", "2"]}], 
      RowBox[{
       RowBox[{"k", " ", 
        SuperscriptBox["l", "2"]}], "+", 
       RowBox[{"g", " ", "l", " ", "m"}]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.777061453885091*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"matrizdinamica", "=", 
   RowBox[{"kases", "-", 
    RowBox[{"w", "*", "masas"}]}]}]}]], "Input",
 CellChangeTimes->{{3.7770614628748417`*^9, 3.777061478297044*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"k", " ", 
       SuperscriptBox["l", "2"]}], "+", 
      RowBox[{"g", " ", "l", " ", "m"}], "-", 
      RowBox[{
       SuperscriptBox["l", "2"], " ", "m", " ", "w"}]}], ",", 
     RowBox[{
      RowBox[{"-", "k"}], " ", 
      SuperscriptBox["l", "2"]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{
      RowBox[{"-", "k"}], " ", 
      SuperscriptBox["l", "2"]}], ",", 
     RowBox[{
      RowBox[{"k", " ", 
       SuperscriptBox["l", "2"]}], "+", 
      RowBox[{"g", " ", "l", " ", "m"}], "-", 
      RowBox[{
       SuperscriptBox["l", "2"], " ", "m", " ", "w"}]}]}], "}"}]}], 
  "}"}]], "Output",
 CellChangeTimes->{3.777061494979944*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"MatrixForm", "[", "%", "]"}]], "Input",
 CellChangeTimes->{{3.777061496058084*^9, 3.7770614995963173`*^9}}],

Cell[BoxData[
 TagBox[
  RowBox[{"(", "\[NoBreak]", GridBox[{
     {
      RowBox[{
       RowBox[{"k", " ", 
        SuperscriptBox["l", "2"]}], "+", 
       RowBox[{"g", " ", "l", " ", "m"}], "-", 
       RowBox[{
        SuperscriptBox["l", "2"], " ", "m", " ", "w"}]}], 
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       SuperscriptBox["l", "2"]}]},
     {
      RowBox[{
       RowBox[{"-", "k"}], " ", 
       SuperscriptBox["l", "2"]}], 
      RowBox[{
       RowBox[{"k", " ", 
        SuperscriptBox["l", "2"]}], "+", 
       RowBox[{"g", " ", "l", " ", "m"}], "-", 
       RowBox[{
        SuperscriptBox["l", "2"], " ", "m", " ", "w"}]}]}
    },
    GridBoxAlignment->{
     "Columns" -> {{Center}}, "ColumnsIndexed" -> {}, "Rows" -> {{Baseline}}, 
      "RowsIndexed" -> {}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.7]}, 
        Offset[0.27999999999999997`]}, "ColumnsIndexed" -> {}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}, "RowsIndexed" -> {}}], "\[NoBreak]", ")"}],
  Function[BoxForm`e$, 
   MatrixForm[BoxForm`e$]]]], "Output",
 CellChangeTimes->{3.777061500014189*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Det", "[", "%", "]"}]], "Input",
 CellChangeTimes->{{3.777061503015318*^9, 3.777061505227606*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"2", " ", "g", " ", "k", " ", 
   SuperscriptBox["l", "3"], " ", "m"}], "+", 
  RowBox[{
   SuperscriptBox["g", "2"], " ", 
   SuperscriptBox["l", "2"], " ", 
   SuperscriptBox["m", "2"]}], "-", 
  RowBox[{"2", " ", "k", " ", 
   SuperscriptBox["l", "4"], " ", "m", " ", "w"}], "-", 
  RowBox[{"2", " ", "g", " ", 
   SuperscriptBox["l", "3"], " ", 
   SuperscriptBox["m", "2"], " ", "w"}], "+", 
  RowBox[{
   SuperscriptBox["l", "4"], " ", 
   SuperscriptBox["m", "2"], " ", 
   SuperscriptBox["w", "2"]}]}]], "Output",
 CellChangeTimes->{3.777061505900141*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Solve", "[", 
  RowBox[{
   RowBox[{"%", "\[Equal]", "0"}], ",", "w"}], "]"}]], "Input",
 CellChangeTimes->{{3.777061509176305*^9, 3.777061513718927*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"w", "\[Rule]", 
     FractionBox["g", "l"]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"w", "\[Rule]", 
     FractionBox[
      RowBox[{
       RowBox[{"2", " ", "k", " ", "l"}], "+", 
       RowBox[{"g", " ", "m"}]}], 
      RowBox[{"l", " ", "m"}]]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.777061514709793*^9}]
}, Open  ]],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", 
  RowBox[{"Eigensystem", "[", 
   RowBox[{"{", 
    RowBox[{"kases", ",", "masas"}], "}"}], "]"}]}]], "Input",
 CellChangeTimes->{{3.7770615686917562`*^9, 3.777061587480324*^9}}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox["g", "l"], ",", 
     FractionBox[
      RowBox[{
       RowBox[{"2", " ", "k", " ", "l"}], "+", 
       RowBox[{"g", " ", "m"}]}], 
      RowBox[{"l", " ", "m"}]]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"1", ",", "1"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", "1"}], ",", "1"}], "}"}]}], "}"}]}], "}"}]], "Output",
 CellChangeTimes->{3.7770615884568663`*^9}]
}, Open  ]]
},
WindowSize->{828, 669},
WindowMargins->{{Automatic, 109}, {-6, Automatic}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 284, 9, 32, "Input"],
Cell[CellGroupData[{
Cell[867, 33, 447, 15, 57, InheritFromParent],
Cell[1317, 50, 923, 29, 51, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2277, 84, 398, 11, 77, "Input"],
Cell[2678, 97, 323, 11, 37, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3038, 113, 129, 2, 32, "Input"],
Cell[3170, 117, 741, 22, 66, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[3948, 144, 675, 22, 32, "Input"],
Cell[4626, 168, 603, 21, 37, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[5266, 194, 127, 2, 32, "Input"],
Cell[5396, 198, 1031, 32, 66, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[6464, 235, 223, 5, 55, "Input"],
Cell[6690, 242, 758, 26, 37, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7485, 273, 131, 2, 32, "Input"],
Cell[7619, 277, 1187, 36, 66, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8843, 318, 122, 2, 32, "Input"],
Cell[8968, 322, 597, 17, 34, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[9602, 344, 178, 4, 32, "Input"],
Cell[9783, 350, 393, 13, 50, "Output"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10213, 368, 225, 5, 55, "Input"],
Cell[10441, 375, 512, 18, 50, "Output"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
