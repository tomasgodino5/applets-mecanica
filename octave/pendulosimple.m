% script pendulosimple.m

# Prevent Octave from thinking that this
# is a function file:
1;

# Sistema EDO para el pendulo simple
function xdot = f (x,t)
w2=1; # w^2 = g/l 
xdot(1)=x(2); #d ang/ dt = velang
xdot(2)=-w2*sin(x(1)); #d velang/ dt = w^2 sin(ang)
endfunction

#condiciones iniciales
ang0=0.99*pi;
velang0=0.0;

x = lsode ("f", [ang0; velang0], (t = linspace (0, 100, 200)'));

ang=x(:,1);
velang=x(:,2);

plot(t,ang,"-*",t,velang,"-o")
xlabel ("t")
ylabel ("")
legend(["angulo";"velocidad angular"])

