// Instalar la libreria boost odeint
// Para Compilar
// g++ pendulo.cpp

#include <iostream>
#include <boost/array.hpp>

#include <boost/numeric/odeint.hpp>

using namespace std;
using namespace boost::numeric::odeint;

const double g = 9.8;
const double l = 1.0;

typedef boost::array< double , 2 > state_type;

void pendulo( const state_type &x , state_type &dxdt , double t )
{
    // Pendulo:
    // ang ==> x[0]
    // velang ==> x[1]
    // d ang/dt = velang ==> dxdt[0]
    // d velang/dt = -(g/l) sin(ang) ==> dxdt[1]        

    dxdt[0] = x[1];
    dxdt[1] = (-g/l)*sin(x[0]);
}

void write_pendulo( const state_type &x , const double t )
{
    // Energia = m l^2 velang^2 /2 - m g l cos(ang)     
    // ang ==> x[0], velang ==> x[1]
        	
    double Energia=x[1]*x[1]*l*l/2.0 - g*l*cos(x[0]);
    cout << t << '\t' << x[0] << '\t' << x[1] << " " << Energia << endl;
}

int main(int argc, char **argv)
{
    state_type x = { M_PI*0.98, 0.0}; // condiciones iniciales {ang0,velang0}
    integrate( pendulo , x , 0.0 , 25.0 , 0.1 , write_pendulo );
}
