# correr en gnuplot (ejemplo)
# theta0=3.14; dthetadt0=0.0; load "pendulosimple.gnu"

# correr desde la linea de comandos (ejemplo)
# gnuplot -e "theta0=3.14; dthetadt0=0.0" pendulosimple.gnu -p

# parametros del pendulo
g=9.8
m=0.1
l=1.0

t=0
dt=0.005

# condiciones iniciales
#theta0=3.14
#dthetadt0=0.0

x = theta0
v = dthetadt0

set print "solucion.dat"
do for[i=0:5000]{

	# energia 
	E = m*l**2*v**2*0.5 - m*g*l*cos(x)

	# metodo leapfrog (el mas simple)
	v = v - dt*(g/l)*sin(x)
	x = x + v*dt	
	
	t=t+dt;

	if(i%5==0){print t,x,v,E}
}
set print 

set multi lay 1,2
set xlabel 't'
plot "solucion.dat" u 1:2 t "theta" w lp,\
'' u 1:3 t "dthetadt" w lp, \
'' u 1:4 t "Energía" w lp,0

set xlabel 'theta'
set ylabel 'd theta/d t'
plot "solucion.dat" u 2:3 t "theta" w lp
unset multi


